add_library(penna STATIC genome.cpp animal.cpp population.cpp fish_population.cpp)

target_include_directories(penna PUBLIC ${PROJECT_SOURCE_DIR}/src)
