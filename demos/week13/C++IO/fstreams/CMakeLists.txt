cmake_minimum_required(VERSION 3.1)

project(PT1_week13_CppIO_fstreams)

# use C++11 (globally)
set(CMAKE_CXX_STANDARD 11)          # set standard to C++11
set(CMAKE_CXX_STANDARD_REQUIRED ON) # requires C++11
set(CMAKE_CXX_EXTENSIONS OFF)       # disables extensions such as -std=g++XX

add_executable(file_io file_io.cpp)
add_executable(file_io_bin file_io_bin.cpp)
