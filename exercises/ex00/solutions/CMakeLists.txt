# Programming Techniques for Scientific Simulations I
# HS 2021
# Exercise 0
cmake_minimum_required (VERSION 3.1)
project (ex00 LANGUAGES CXX)

set (CMAKE_CXX_STANDARD 17)
set (CMAKE_CXX_STANDARD_REQUIRED TRUE)
set (CMAKE_CXX_EXTENSIONS FALSE)

add_executable (fibonacci  fibonacci.cpp)
add_executable (gcd        gcd.cpp)
add_executable (merge_sort merge_sort.cpp)
