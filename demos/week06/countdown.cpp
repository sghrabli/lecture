#include <chrono>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <thread>

void sleep(unsigned int seconds) {
  std::this_thread::sleep_for(std::chrono::seconds(seconds));
}

void countdown(int t) {
  while ( true ) {
    std::string mess = "We are back in ";
    if ( t < 0 ) mess = "We are late since ";
    std::cout << mess
              << std::setfill('0') << std::setw(2) << std::abs(t) / 60
              << ":"
              << std::setfill('0') << std::setw(2) << std::abs(t) % 60
              << "...\r"
              << std::flush;;
    sleep(1);
    t -= 1;
  }
  std::cout << std::endl;
}

int main() {
  countdown(60);
}
