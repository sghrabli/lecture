class A:
    def __init__(self):
        self.a = 1

    def print_A(self):
        print(self.a)

class B(A):
    def __init__(self):
        super().__init__() # self.a == 1
        self.b = 2

    def print_B(self):
        print(self.b)

b = B()
b.print_A() # 1
b.print_B() # 2
