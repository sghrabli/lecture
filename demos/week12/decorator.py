def dec(f):
    def inner():
        print('Hello', end=' ')
        f()
    return inner

@dec # f = dec(f)
def f():
    print('World')

f() # Hello World
