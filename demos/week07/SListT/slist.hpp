#ifndef SLIST_HPP
#define SLIST_HPP

template <typename T>
struct node {
  T value;       // the element
  node<T> *next; // the next node
};

template <typename T>
struct node_iterator {
  node<T>* p;
  node_iterator(node<T>* q) : p(q) {}
  node_iterator<T>& operator++() { p=p->next; return *this; }
  T* operator->() { return &(p->value); }
  T& operator*() { return p->value; }
  bool operator!=(const node_iterator<T>& x) { return p!=x.p; }
  // more operators missing
};

// (too!!!) simple list
template<typename T>
struct slist {
  using iterator = node_iterator<T>;
  slist() : first(nullptr) {}
  iterator begin() { return iterator(first); }
  iterator end() { return iterator(nullptr); }
  void addElement(T const& val) {
    node<T>* n = new node<T>();
    n->value = val;
    n->next = first;
    first = n;
  }
  node<T>* first;
};

#endif /* SLIST_HPP */
